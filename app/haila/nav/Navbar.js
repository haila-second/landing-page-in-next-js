'use-client'

import React, { useState } from 'react'
import logo from "../../imgs/logo.png";
import Image from 'next/image';
import {FaBars, FaTimes} from 'react-icons/fa';

const Navbar = () => {
    const [show, setShow] = useState(false);
  return (
    <div className='font-serif text-black bg-white'>
    <div className="flex justify-between px-10 py-4">
        <div className="cursor-pointer">
            <Image 
            src={logo}
            alt="website Logo"
            width={70}
            height={70} 
                className='rounded-full'
            />
        </div>
        <div className='cursor-pointer md:hidden' onClick={()=>setShow(!show)}>
            {show ? <FaTimes />:<FaBars />}
        </div>
        <div className='pt-6 md:flex hidden'>
            <ul className="flex px-10 cursor-pointer">
                <li className="px-2 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">Home</li>
                <li className="px-2 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">Menu</li>
                 {/* eslint-disable-next-line react/no-unescaped-entities */}
                <li className="px-2 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">What's New</li>
                <li className="px-2 font-bold text-lg">Contact</li>
            </ul>
        </div>
        {show ? (
            <div className='pt-6 flex md:hidden'>
            <ul className="flex flex-col px-10 cursor-pointer">
                <li className="py-4 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">Home</li>
                <li className="py-4 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">Menu</li>
                 {/* eslint-disable-next-line react/no-unescaped-entities */}
                <li className="py-4 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">What's New</li>
                <li className="py-4 font-bold text-lg hover:text-green-900 duration-500 ease-in hover:border-b-4 hover:border-slate-900">Contact</li>
            </ul>
        </div>
        ):null}
    </div>
    </div>
  )
}

export default Navbar