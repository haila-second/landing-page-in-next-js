'use client'

/* eslint-disable react/no-unescaped-entities */
import React from 'react'
import green from '../../imgs/green.png';
import face from '../../imgs/facebook.png'
import insta from '../../imgs/instagram.png';
import twit from '../../imgs/twitter.png';
import Image from 'next/image';


const Hero = () => {
  return (
    <div className="bg-white text-black font-serif overflow-x-hidden">
        <div  className="flex flex-col lg:flex-row justify-center lg:justify-between  px-20 py-10 z-20">
            <div data-aos="slide-right" className="pt-20 z-30">
                <h1 className="text-5xl py-4">It's not just coffee <br/>
                it's <span className="font-extrabold text-[#cbcb2c]  md:text-green-900 ">Starbucks</span></h1>
                <p>Coffee is a popular beverage that is enjoyed by <br/>
                 millions of people around the world. It's made from<br/>
                  the roasted seeds of the Coffee plant and comes in a <br/>
                   variety of different forms, including espresso,<br/>
                    cappuccino, and latte.</p>
                    <button className='z-30 font-bold text-white bg-[#cbcb2c] md:bg-green-900 h-10 w-44 px-4 my-6 rounded-3xl'>Learn More</button>
            </div>
            <div data-aos="slide-left" className="flex justify-between z-30">
             <div className='pt-32 lg:pt-0'>
                    <Image
                src={green}
                alt="green cup of coffee"
                width={400}
                height={400} />
             </div>
             <div className="flex text-black flex-col -pt-24 lg:pt-40">
                <Image
                src={face}
                alt="facebook coffee page"
                width={20}
                height={20} className="py-6 hover:text-white hover:bg-white cursor-pointer hover:transform hover:translate-x-6 duration-500"/>
                <Image
                src={insta}
                alt="instagram coffee page"
                width={20}
                height={20} className="py-6 hover:text-white hover:bg-white cursor-pointer hover:transform hover:translate-x-6 duration-500"/>
                <Image
                src={twit}
                alt="twitter coffee page"
                width={20}
                height={20} className="py-6 hover:text-white hover:bg-white cursor-pointer hover:transform hover:translate-x-6 duration-500"/>
            </div>
            </div>
           
        </div>
    </div>
  )
}

export default Hero