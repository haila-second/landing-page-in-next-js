import React from 'react'
import tim1 from '../../imgs/thumb1.png'
import tim2 from '../../imgs/thumb2.png'
import tim3 from '../../imgs/thumb3.png';
import Image from 'next/image';

const Footer = () => {
  return (
    <div className="bg-white overflow-y-hidden">
    <div data-aos="slide-up" className='justify-center flex pb-20 z-30 overflow-y-hidden'>
    <Image
    src={tim1}
    alt="First" 
    width={80}
    height={80} className='px-4 hover:text-white cursor-pointer hover:transform hover:translate-y-6 duration-500'/>
    <Image
    src={tim2}
    alt="First" 
    width={80}
    height={80} className='px-4 hover:text-white cursor-pointer hover:transform hover:translate-y-6 duration-500'/>
    <Image
    src={tim3}
    alt="First" 
    width={80}
    height={80} className='px-4 hover:text-white cursor-pointer hover:transform hover:translate-y-6 duration-500'/>
    </div>
    </div>
  )
}

export default Footer