'use client'

import Image from 'next/image'
import Navbar from './haila/nav/Navbar'
import HEAD from 'next/head';
import Hero from './haila/hero/Hero';
import Footer from './haila/footer/Footer';
import AOS from 'aos';
import 'aos/dist/aos.css';
import { useEffect } from 'react';

export default function Home() {

  useEffect(()=>{
    AOS.init({duration:2000})
  },[]);
  return (
    <div className='overflow-y-hidden bg-white overflow-x-hidden'>
    <HEAD>
      <title>Landing Page</title>
    </HEAD>
    <div>
    <Navbar />
    <Hero />
    <Footer />
    </div> 
    </div>
  )}